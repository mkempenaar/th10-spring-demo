# A simple Java Spring web application #

## The following instructions are for use with Netbeans 8.* ##

TODO

## The following instructions are for use with IntelliJ IDEA 15.* ##

Cloning and building the project from Bitbucket:

* File -> New Project -> Version Control -> Git
    * Enter URL: http://bitbucket.org/harmbrugge/spring-demo.git
    * Click the 'Link Gradle project' popup after cloning the project
        * Leave everything default (two popups) and wait for the project to build / download dependencies

!['Unlinked Gradle project'](https://bitbucket.org/mkempenaar/th10-spring-demo/raw/master/images/gradle.png width="400")

Setting up the project:

* File -> Project Structure
    * Change the SDK on the 'Project' and 'Modules' page to Java 7 or 8

![Setting up the SDK](https://bitbucket.org/mkempenaar/th10-spring-demo/raw/master/images/sdk.png width="500")

Deploying the demo application:

* Run -> Edit Configurations
    * Button 'Add new configuration'
    * Select 'Tomcat Server' -> 'Local'
        * 'Deployment' tab -> button 'Add'
            * Select the top 'War' file (artefact)

![Selecting the artefact to deploy](https://bitbucket.org/mkempenaar/th10-spring-demo/raw/master/images/artefacts.png width="500")

Press the 'Play' button (top right) and wait for the project to compile, deploy and launch your default / selected browser