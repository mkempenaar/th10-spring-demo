package com.example;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

/**
 * Created by hbrugge on 12-1-16.
 */
@Controller
public class HomeController {

    @RequestMapping("/")
    public String showHome(Model model) {

        model.addAttribute("message", "Hallo");


        return "home";
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public String showUserForm(User user) {

        return "user";
    }

    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public String doUserForm(@Valid User user, BindingResult result) {

        if (result.hasErrors()) {
            return "user";
        }

        System.out.println(user);

        return "redirect:/";
    }
}
